﻿using UnityEngine;
using System.Collections;

public class VideoPlayerController : MonoBehaviour {

	public void Play (){
		PhotonNetwork.Instantiate("Play", Vector3.zero, Quaternion.identity, 0);
	}
/*
	public void Stop (){
		PhotonNetwork.Instantiate("Stop", Vector3.zero, Quaternion.identity, 0);
	}
*/
	public void Pause (){
		PhotonNetwork.Instantiate("Pause", Vector3.zero, Quaternion.identity, 0);
	}

	public void Next (){
		PhotonNetwork.Instantiate("Next", Vector3.zero, Quaternion.identity, 0);
	}

	public void Previous (){
		PhotonNetwork.Instantiate("Previous", Vector3.zero, Quaternion.identity, 0);
	}
}
