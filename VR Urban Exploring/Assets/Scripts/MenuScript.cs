using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//This file controls functions related to the main menu, including functions
//that load videos and control when buttons are enabled/disabled

//Use unload to reduce overhead
public class MenuScript : MonoBehaviour {
	//These are set in the inspector
	public MediaPlayerCtrl srcMedia;
	public GameObject PlayerController, Sphere, MenuCanvas, VideoPlayerControls;

	void Start(){
		Sphere.SetActive (false);
		VideoPlayerControls.SetActive(false);
	}

	//This function takes the name of a video as an argument and creates a matching
	//gameobject which is instantiated on every player's game. The players read
	//this object in Update() and launch the appropriate video. We did it this way
	//because of endless bugs with RPCs and other official communication methods.
	//It is called by the main menu buttons
	public void LoadScene (string destination) {
		PhotonNetwork.Instantiate(destination, Vector3.zero, Quaternion.identity, 0);
		//MenuCanvas.GetComponent<Canvas> ().enabled = false;
	}

	//This methods checks for the creation of signalling gameobjects and launches
	//the appropriate video.
	void Update(){
		//Launch video 1, destroy signalling gameobject
		if(GameObject.Find("video1(Clone)") != null){
			moveToSphere();
			srcMedia.Load ("video1.mp4");
			Destroy(GameObject.Find("video1(Clone)"));
		}
		//Launch video 2, destroy signalling gameobject
		if(GameObject.Find("video2(Clone)") != null){
			moveToSphere();
			srcMedia.Load ("video2.mp4");
			Destroy(GameObject.Find("video2(Clone)"));
		}
		//Launch video 3, destroy signalling gameobject
		if(GameObject.Find("video3(Clone)") != null){
			moveToSphere();
			srcMedia.Load ("video3.mp4");
			Destroy(GameObject.Find("video3(Clone)"));
		}
		//Launch video 4, destroy signalling gameobject
		if(GameObject.Find("video4(Clone)") != null){
			moveToSphere();
			srcMedia.Load ("video4.mp4");
			Destroy(GameObject.Find("video4(Clone)"));
		}
	}

	void moveToSphere(){
		//MenuCanvas.SetActive (false);
		Sphere.SetActive (true);
		//Makes the controls only visible to the leader
		if(GameObject.Find("IamLeader(Clone)") != null)
			VideoPlayerControls.SetActive(true);
		PlayerController.transform.position = new Vector3 (0, 2, -60);
	}
	//TODO: change all finds to this
	void setInteractable(string obj, bool which){
		  GameObject.Find(obj).GetComponent<Button>().interactable = which;
	}

	//This function activates when the team leader button is selected, it creates
	//a local team leader object and a global not team leader object
	public void MakeLeader(){
	  Instantiate(Resources.Load("IamLeader"));
	  PhotonNetwork.Instantiate("IamNotLeader", Vector3.zero, Quaternion.identity, 0);
	}

	public void MakeNotLeader(){
	  Destroy(GameObject.Find("IamLeader(Clone)"));
	  PhotonNetwork.Destroy(GameObject.Find("IamNotLeader(Clone)"));
	}
}
