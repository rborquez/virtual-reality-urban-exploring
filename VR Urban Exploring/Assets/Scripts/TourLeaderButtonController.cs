﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//This file is attached
public class TourLeaderButtonController : MonoBehaviour {
public GameObject TLImage, TRImage, BLImage, BRImage;
public Texture TLlight, TLgrey, TRlight, TRgrey, BLlight, BLgrey, BRlight, BRgrey;

	void Start(){
		//This checks if the player is the leader and disables video selection if
		//not leader. It is called when MakeLeader() is activated
		if(GameObject.Find("IamLeader(Clone)")){
			GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = false;
			GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = true;
			GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = true;
			GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = true;
			GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = true;
			GameObject.Find("StopTourLeaderButton").GetComponent<Button>().interactable = true;
			//This changes the images from unactive to active
			/*
			TLImage.GetComponent<RawImage>().texture = (Texture)TLlight;
			TRImage.GetComponent<RawImage>().texture = (Texture)TRlight;
			BLImage.GetComponent<RawImage>().texture = (Texture)BLlight;
			BRImage.GetComponent<RawImage>().texture = (Texture)BRlight;
			*/
		}
		else{
			GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = false;
			GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = false;
			GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = false;
			GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = false;
			GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = false;
			}
	}
	void OnDestroy(){
		//This checks if the tour leader has given up their position and reenables
		// video selection for all players.It is called when  MakeNotLeader() is
		// activated
		GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = true;
		GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = false;
		GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = false;
		GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = false;
		GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = false;
	  GameObject.Find("StopTourLeaderButton").GetComponent<Button>().interactable = false;
		//This sets the images back to greyed out
		/*
		TLImage.GetComponent<RawImage>().texture = (Texture)TLgrey;
		TRImage.GetComponent<RawImage>().texture = (Texture)TRgrey;
		BLImage.GetComponent<RawImage>().texture = (Texture)BLgrey;
		BRImage.GetComponent<RawImage>().texture = (Texture)BRgrey;
		*/
	}
}
