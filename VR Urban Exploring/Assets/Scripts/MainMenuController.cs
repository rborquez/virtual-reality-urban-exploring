﻿using UnityEngine;
using System.Collections;
//This file contains functions that controls the sync button that players see
//before the main menu and when the main menu becomes visible
//This file is disabled and not responsible for much until we decide we
//need a screen before the main menu
public class MainMenuController : MonoBehaviour {
	public Canvas InstructionCanvas;
	public GameObject PlayerController, Pivot, MenuCanvas;

	void Start (){
		//Needed to instantiate an arrow in case someone goes back to main menu
		//PhotonNetwork.Instantiate("NetworkedPlayer", Vector3.zero, Quaternion.identity, 0);
		//To disable the video selection while they are reading instructions
		//MenuCanvas.SetActive (false);
		//InstructionCanvas.GetComponent<Canvas> ().enabled = false;
	}

	//When the Sync button is pressed it will disable the instructions and enable the video selection.
	public void OnSync () {
		MenuCanvas.SetActive (true);
		InstructionCanvas.GetComponent<Canvas> ().enabled = false;
	}
}
