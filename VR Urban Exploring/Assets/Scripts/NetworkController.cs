﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NetworkController : MonoBehaviour
{
	string _room = "VR Urban Exploring";

	void Start(){
		PhotonNetwork.ConnectUsingSettings("0.1");
	}

	void OnJoinedLobby(){
		Debug.Log("joined lobby");
		RoomOptions roomOptions = new RoomOptions() { };
		PhotonNetwork.JoinOrCreateRoom(_room, roomOptions, TypedLobby.Default);
	}

	void OnJoinedRoom(){
		PhotonNetwork.Instantiate("NetworkedPlayer", Vector3.zero, Quaternion.identity, 0);
		//This checks if someone else is tourleader
		if(!GameObject.Find("IamNotLeader(Clone)"))
			GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = true;
	}
}
