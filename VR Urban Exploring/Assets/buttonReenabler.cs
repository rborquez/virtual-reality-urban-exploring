﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class buttonReenabler : MonoBehaviour {

	void Start(){
		//This checks if the player is the leader and disables video selection for
		// the other player ie activated MakeLeader()
		if(
			GameObject.Find("IamNotLeader(Clone)") &&
			!GameObject.Find("IamLeader(Clone)")
			){
				GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = false;
				GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = false;
			}
	}
	void OnDestroy(){
		//This checks if the tour leader has given up their position and reenables
		// video selection for all players ie activated MakeNotLeader()
		GameObject.Find("TLVideoButton").GetComponent<Button>().interactable = true;
		GameObject.Find("TRVideoButton").GetComponent<Button>().interactable = true;
		GameObject.Find("BLVideoButton").GetComponent<Button>().interactable = true;
		GameObject.Find("BRVideoButton").GetComponent<Button>().interactable = true;
		GameObject.Find("TourLeaderButton").GetComponent<Button>().interactable = true;
	}
}
